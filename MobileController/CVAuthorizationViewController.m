//
//  CVAuthorizationViewController.m
//  MobileController
//
//  Created by Ivan Tsaryov on 01/05/17.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import "CVAuthorizationViewController.h"

// Service
#import "CVService.h"
#import "CVLoginRequest.h"
#import "CVLoginResponse.h"

@interface CVAuthorizationViewController () <UIScrollViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *loginField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) UIActivityIndicatorView *loadingView;
@property (strong, nonatomic) UIView *shadowView;

@property (strong, nonatomic) CVService *service;

@end

@implementation CVAuthorizationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.scrollView.delegate = self;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    
    self.loginField.delegate = self;
    self.passwordField.delegate = self;
    
    self.service = [CVService new];
}

- (IBAction)onLoginButtonTap:(UIButton *)sender {
    if (![self validateLogin:self.loginField.text password:self.passwordField.text]) {
        return;
    }
    
    CVLoginRequest *req = [CVLoginRequest new];
    req.login = self.loginField.text;
    req.password = self.passwordField.text;
    
    [self eraseToken];
    [self showLoadingView];
    [self.service executeWithRequest:req responseClass:[CVLoginResponse class] success:^(CVResponse *response, NSData *data) {
        NSString *token = ((CVLoginResponse *)response).data;
        
        [self hideLoadingView];
        
        if (token.length > 0) {
            [self saveToken:token];
            
            [self performSegueWithIdentifier:@"ShowChat" sender:self];
        } else {
            [self clientErrorHappened:((CVLoginResponse *)response).responseDescription];
        }
    } failure:^(NSError *error) {
        [self performSegueWithIdentifier:@"ShowChat" sender:self];
        
        [self clientErrorHappened:[error localizedDescription]];
    }];
}

#pragma - Spell checking

- (BOOL)validateLogin:(NSString *)login password:(NSString *)password
{
    if (login.length > 0 && password.length > 0) {
        return YES;
    } else {
        [self clientErrorHappened:@"Заполните оба поля!"];
    }
    
    return NO;
}

#pragma - Other

- (void)eraseToken
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:@"" forKey:@"kCVToken"];
}

- (void)saveToken:(NSString *)token
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:token forKey:@"kCVToken"];
}

- (void)clientErrorHappened:(NSString *)errorDescription
{
    [self hideLoadingView];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Ошибка"
                                                                             message:errorDescription
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:nil]];
    [self.view endEditing:YES];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //[self.view endEditing:YES];
    
    // Disable horizontal scrolling
    if (scrollView.contentOffset.x > 0 || scrollView.contentOffset.x < 0) {
        CGPoint contentOffset = scrollView.contentOffset;
        contentOffset.x = 0;
        
        scrollView.contentOffset = contentOffset;
    }
}

#pragma mark - Loader

- (void)showLoadingView {
    if (!self.loadingView) {
        self.loadingView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.loadingView.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        self.loadingView.center = self.view.center;
        
        self.shadowView = [[UIView alloc] initWithFrame:self.view.bounds];
        UIColor *backgroundColor = [UIColor colorWithRed:216/255.0
                                                   green:216/255.0
                                                    blue:216/255.0
                                                   alpha:1.0];
        
        [self.shadowView setBackgroundColor:[backgroundColor colorWithAlphaComponent:0.5f]];
        
        [self.view addSubview:self.loadingView];
        [self.loadingView bringSubviewToFront:self.view];
    }
    
    [self.view addSubview:self.shadowView];
    [self.loadingView startAnimating];
}

- (void)hideLoadingView {
    [self.shadowView removeFromSuperview];
    [self.loadingView stopAnimating];
}

@end
