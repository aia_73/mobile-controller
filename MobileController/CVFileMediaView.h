//
//  CVFileMediaView.h
//  MobileController
//
//  Created by Ivan Tsaryov on 15/05/17.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVFileMediaView : UIView

@property (weak, nonatomic) IBOutlet UILabel *fileNameLabel;

@end
