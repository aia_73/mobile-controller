//
//  CVCommandRequest.h
//  MobileController
//
//  Created by Ivan Tsaryov on 10/05/17.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import "CVRequest.h"

@interface CVCommandRequest : CVRequest

@property (strong, nonatomic) NSString *message;

@end
