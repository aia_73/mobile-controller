//
//  CVFileMediaItem.m
//  MobileController
//
//  Created by Ivan Tsaryov on 15/05/17.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import "CVFileMediaItem.h"
#import "CVFileMediaView.h"
#import <JSQMessagesViewController/JSQMessagesMediaViewBubbleImageMasker.h>

@interface CVFileMediaItem ()

@end

@implementation CVFileMediaItem

- (instancetype)initWithFileURL:(NSString *)fileURL
{
    self = [super init];
    
    if (self) {
        self.fileURL = fileURL;
    }
    
    return self;
}

#pragma mark - JSQMessageMediaData protocol

- (CGSize)mediaViewDisplaySize
{
    CGRect labelRect = [self.fileURL.lastPathComponent
                        boundingRectWithSize:CGSizeMake(160, CGFLOAT_MAX)
                        options:NSStringDrawingUsesLineFragmentOrigin
                        attributes:@{
                                     NSFontAttributeName : [UIFont systemFontOfSize:15]
                                     }
                        context:nil];
    
    return CGSizeMake(78.0f + labelRect.size.width, 32.0f + labelRect.size.height);
}

- (UIView *)mediaView
{
    CVFileMediaView *mediaView = [[[NSBundle mainBundle] loadNibNamed:@"CVFileMediaView" owner:self options:nil] objectAtIndex:0];
    
    CGRect frame = mediaView.frame;
    
    mediaView.fileNameLabel.text = self.fileURL.lastPathComponent;
    
    CGRect labelRect = [mediaView.fileNameLabel.text
                        boundingRectWithSize:CGSizeMake(160, CGFLOAT_MAX)
                        options:NSStringDrawingUsesLineFragmentOrigin
                        attributes:@{
                                     NSFontAttributeName : [UIFont systemFontOfSize:15]
                                     }
                        context:nil];
    
    frame.size.height = labelRect.size.height + 32;
    frame.size.width = labelRect.size.width + 78;
    mediaView.frame = frame;
    
    
    [JSQMessagesMediaViewBubbleImageMasker applyBubbleImageMaskToMediaView:mediaView isOutgoing:self.appliesMediaViewMaskAsOutgoing];
    
    return mediaView;
}

- (NSUInteger)mediaHash
{
    return super.hash;
}

@end
