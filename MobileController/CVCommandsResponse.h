//
//  CVCommandsResponse.h
//  MobileController
//
//  Created by Ivan Tsaryov on 10/05/17.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import "CVResponse.h"

@interface CVCommandsResponse : CVResponse

@property (strong, nonatomic) NSString *code;

@property (strong, nonatomic) NSString *responseDescription;

@property (strong, nonatomic) NSString *dataType;

@property (strong, nonatomic) NSString *data;

@end
