//
//  CVLoginRequest.h
//  MobileController
//
//  Created by Ivan Tsaryov on 01/05/17.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import "CVRequest.h"

@interface CVLoginRequest : CVRequest

@property (strong, nonatomic) NSString *login;

@property (strong, nonatomic) NSString *password;

@end
