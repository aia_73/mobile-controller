//
//  CVService.h
//  MobileController
//
//  Created by Ivan Tsaryov on 30/04/17.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CVRequest;
@class CVResponse;

typedef void(^CVServiceSuccessBlock)(CVResponse *response, NSData *data);
typedef void(^CVServiceFailureBlock)(NSError *error);
typedef void(^CVServiceDownloadFileSuccessBlock)(NSString *mimeType, NSString *fileURL);

@interface CVService : NSObject

- (void)executeWithRequest:(CVRequest *)request
             responseClass:(Class)responseClass
                   success:(CVServiceSuccessBlock)success
                   failure:(CVServiceFailureBlock)failure;

- (void)executeDownloadingWithURL:(NSString *)url
                          success:(CVServiceDownloadFileSuccessBlock)success
                          failure:(CVServiceFailureBlock)failure;

- (void)executeDownloadingTaskWithURL:(NSString *)url
                              success:(CVServiceDownloadFileSuccessBlock)success
                              failure:(CVServiceFailureBlock)failure;

- (void)executeUploadingWithPath:(NSString *)path
                         success:(CVServiceSuccessBlock)successBlock
                         failure:(CVServiceFailureBlock)failure;

@end
