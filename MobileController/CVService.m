//
//  CVService.m
//  MobileController
//
//  Created by Ivan Tsaryov on 30/04/17.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import "CVService.h"
#import "CVRequest.h"
#import "CVResponse.h"

@import MobileCoreServices;

@interface CVService ()

@property (strong, nonatomic) NSString *serviceEndpoint;

@end

@implementation CVService

- (NSString *)serviceEndpoint
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *endpoint = [defaults stringForKey:@"kCVServerEndpoint"];
    
    return endpoint? [NSString stringWithFormat:@"http://%@/", endpoint] : @"http://192.168.0.103:8181/";
}

- (void)executeWithRequest:(CVRequest *)request
             responseClass:(Class)responseClass
                   success:(CVServiceSuccessBlock)success
                   failure:(CVServiceFailureBlock)failure
{
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", self.serviceEndpoint, request.url]];
    
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:URL
                                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                   timeoutInterval:15];
    [req setHTTPMethod:@"POST"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"kCVToken"];
    
    [req setValue:token forHTTPHeaderField:@"authorization"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[[request toJSONData] length]] forHTTPHeaderField:@"Content-Length"];
    
    [req setHTTPBody:[request toJSONData]];
    
    NSURLSession *session = [NSURLSession sharedSession];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:req
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        CVResponse *responseModel;
                                                        
                                                        if (error) {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                if (failure) {
                                                                    failure(error);
                                                                }
                                                            });
                                                        } else {
                                                            responseModel = (CVResponse *)[[responseClass alloc] initWithData:data
                                                                                                                        error:&error];
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                if (success) {
                                                                    success(responseModel, data);
                                                                }
                                                            });
                                                        }
                                                    }];
        [dataTask resume];
    });
}

- (void)executeDownloadingWithURL:(NSString *)url
                          success:(CVServiceDownloadFileSuccessBlock)success
                          failure:(CVServiceFailureBlock)failure
{
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/file/%@", self.serviceEndpoint, url]]];
        
        if (!data) {
            NSError *error = [[NSError alloc] initWithDomain:url
                                                        code:400
                                                    userInfo:@{
                                                               NSLocalizedFailureReasonErrorKey : @"Downloading error"
                                                               }];
            
            failure(error);
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            success(nil, nil);
        });
    });
}

- (void)executeDownloadingTaskWithURL:(NSString *)url
                              success:(CVServiceDownloadFileSuccessBlock)success
                              failure:(CVServiceFailureBlock)failure
{
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURL *completeURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", self.serviceEndpoint, url]];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLRequest *request = [NSURLRequest requestWithURL:completeURL];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURLSessionTask *task = [session downloadTaskWithRequest:request completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (!location) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    failure(error);
                });
                return;
            }
            
            // We've successfully finished the download. Let's save the file
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSString *mimeType = ((NSHTTPURLResponse *)response).MIMEType;
            
            NSArray *URLs = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
            NSURL *documentsDirectory = URLs[0];
            
            NSURL *destinationPath = [documentsDirectory URLByAppendingPathComponent:url];
            NSError *fileManagerError;
            
            // Make sure we overwrite anything that's already there
            [fileManager removeItemAtURL:destinationPath error:NULL];
            BOOL createSuccess = [fileManager copyItemAtURL:location
                                                      toURL:destinationPath
                                                      error:&fileManagerError];
            
            if (createSuccess && success) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    success(mimeType, destinationPath.absoluteString);
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    failure(error);
                });
            }
        }];
        [task resume];
    });
}

#pragma mark - Upload

- (void)executeUploadingWithPath:(NSString *)path
                         success:(CVServiceSuccessBlock)success
                         failure:(CVServiceFailureBlock)failure
{
    NSString *boundary = [self generateBoundaryString];
    
    NSDictionary *params = @{
//                             @"userName" : @"Client",
//                             @"userEmail" : @"client@localhost",
//                             @"userPassword" : @"password"
                             };
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@uploadfile", self.serviceEndpoint]]];
    [request setHTTPMethod:@"POST"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"kCVToken"];
    
    [request setValue:token forHTTPHeaderField:@"authorization"];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSData *httpBody = [self createBodyWithBoundary:boundary
                                         parameters:params paths:@[path]
                                          fieldName:@""];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSLog(@"HTTP BODY: %@", [[NSString alloc] initWithData:httpBody encoding:NSUTF8StringEncoding]);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURLSessionTask *task = [session uploadTaskWithRequest:request fromData:httpBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (error) {
                NSLog(@"error = %@", error);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    failure(error);
                });
            }
            
            NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"result = %@", result);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                success(nil, data);
            });
        }];
        [task resume];
    });
}

#pragma mark - Private

- (NSData *)createBodyWithBoundary:(NSString *)boundary
                        parameters:(NSDictionary *)parameters
                             paths:(NSArray *)paths
                         fieldName:(NSString *)fieldName {
    NSMutableData *httpBody = [NSMutableData data];
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    for (NSString *path in paths) {
        NSString *filename = [path lastPathComponent];
        NSData *data = [NSData dataWithContentsOfFile:path];
        NSString *mimetype = [self mimeTypeForPath:path];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldName, filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}

- (NSString *)mimeTypeForPath:(NSString *)path {
    CFStringRef extension = (__bridge CFStringRef)[path pathExtension];
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
    assert(UTI != NULL);
    
    NSString *mimetype = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    assert(mimetype != NULL);
    
    CFRelease(UTI);
    
    return mimetype;
}

- (NSString *)generateBoundaryString {
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
}

- (NSString *)extensionFromMimeType:(NSString *)mimeType
{
    CFStringRef uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, (__bridge CFStringRef _Nonnull)(mimeType), NULL);
    
    CFStringRef extension = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassFilenameExtension);
    
    return (__bridge NSString *)(extension);
}


@end
