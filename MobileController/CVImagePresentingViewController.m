//
//  CVImagePresentingViewController.m
//  MobileController
//
//  Created by Ivan Tsaryov on 16/05/2017.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import "CVImagePresentingViewController.h"

@interface CVImagePresentingViewController ()

@end

@implementation CVImagePresentingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.imageView.image = self.image;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onCloseButtonTap:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
