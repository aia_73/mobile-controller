//
//  CVFileMediaItem.h
//  MobileController
//
//  Created by Ivan Tsaryov on 15/05/17.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import <JSQMessagesViewController/JSQMessagesViewController.h>
#import <JSQMessagesViewController/JSQMediaItem.h>
#import <JSQMessagesViewController/JSQMessageData.h>


@interface CVFileMediaItem : JSQMediaItem <JSQMessageMediaData>

@property (strong, nonatomic) NSString *fileURL;

- (instancetype)initWithFileURL:(NSString *)fileURL;

@end
