//
//  AppDelegate.h
//  MobileController
//
//  Created by Ivan Tsaryov on 30/04/17.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

