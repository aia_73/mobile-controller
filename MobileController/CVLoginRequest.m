//
//  CVLoginRequest.m
//  MobileController
//
//  Created by Ivan Tsaryov on 01/05/17.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import "CVLoginRequest.h"

@implementation CVLoginRequest

- (NSString *)url
{
    return @"login";
}

@end
