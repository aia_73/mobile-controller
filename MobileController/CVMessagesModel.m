//
//  CVMessagesModel.m
//  MobileController
//
//  Created by Ivan Tsaryov on 30/04/17.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import "CVMessagesModel.h"
#import <JSQMessagesAvatarImageFactory.h>

@implementation CVMessagesModel

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        self.messages = [NSMutableArray new];
        
        JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
        
        self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
        self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleGreenColor]];
        
        JSQMessagesAvatarImage *serverAvatar = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:@"С"
                                                                                          backgroundColor:[UIColor colorWithWhite:0.85f alpha:1.0f]
                                                                                                textColor:[UIColor colorWithWhite:0.60f alpha:1.0f]
                                                                                                     font:[UIFont systemFontOfSize:14.0f]
                                                                                                 diameter:30];
        
        JSQMessagesAvatarImage *clientAvatar = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:@"К"
                                                                                          backgroundColor:[UIColor colorWithWhite:0.85f alpha:1.0f]
                                                                                                textColor:[UIColor colorWithWhite:0.60f alpha:1.0f]
                                                                                                     font:[UIFont systemFontOfSize:14.0f]
                                                                                                 diameter:30];


       
    
        self.avatars = @{
                         @"Сервер" : serverAvatar,
                         @"Клиент" : clientAvatar
                         };
        
        self.users = @[@"Сервер", @"Клиент"];
    }
    
    return self;
}

- (void)addDataMessageWithSender:(NSString *)sender data:(NSData *)data
{
    NSString * sample = [[NSBundle mainBundle] pathForResource:@"jsq_messages_sample" ofType:@"m4a"];
    NSData * audioData = [NSData dataWithContentsOfFile:sample];
    JSQAudioMediaItem *audioItem = [[JSQAudioMediaItem alloc] initWithData:audioData];
    JSQMessage *audioMessage = [JSQMessage messageWithSenderId:sender
                                                   displayName:sender
                                                         media:audioItem];
    [self.messages addObject:audioMessage];

}

@end
