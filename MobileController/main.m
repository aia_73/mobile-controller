//
//  main.m
//  MobileController
//
//  Created by Ivan Tsaryov on 30/04/17.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
