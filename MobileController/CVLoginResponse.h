//
//  CVLoginResponse.h
//  MobileController
//
//  Created by Ivan Tsaryov on 01/05/17.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import "CVCommandsResponse.h"

@interface CVLoginResponse : CVCommandsResponse

@end
