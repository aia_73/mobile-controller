//
//  CVImagePresentingViewController.h
//  MobileController
//
//  Created by Ivan Tsaryov on 16/05/2017.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVImagePresentingViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) UIImage *image;

@end
