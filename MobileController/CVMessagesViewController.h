//
//  CVMessagesViewController.h
//  MobileController
//
//  Created by Ivan Tsaryov on 30/04/17.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import <JSQMessagesViewController/JSQMessagesViewController.h>

#import "JSQMessages.h"

#import "CVMessagesModel.h"

@class CVMessagesViewController;

@protocol JSQDemoViewControllerDelegate <NSObject>

- (void)didDismissJSQDemoViewController:(CVMessagesViewController *)vc;

@end


@interface CVMessagesViewController : JSQMessagesViewController <UIActionSheetDelegate, JSQMessagesComposerTextViewPasteDelegate>

@property (weak, nonatomic) id<JSQDemoViewControllerDelegate> delegateModal;

@property (strong, nonatomic) CVMessagesModel *messagesData;

- (void)closePressed:(UIBarButtonItem *)sender;

@end
