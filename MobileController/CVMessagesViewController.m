//
//  CVMessagesViewController.m
//  MobileController
//
//  Created by Ivan Tsaryov on 30/04/17.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import "CVMessagesViewController.h"
#import "CVImagePresentingViewController.h"

// iOS Modules
#import <Photos/Photos.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>

// Helper
#import "CVMacros.h"
#import "CVFileMediaItem.h"
#import <JSQMessagesViewController/JSQMessageMediaData.h>

// Service
#import "CVService.h"
#import "CVCommandRequest.h"
#import "CVCommandsResponse.h"

@interface CVMessagesViewController () <UIImagePickerControllerDelegate, MPMediaPickerControllerDelegate, UINavigationControllerDelegate, UIDocumentInteractionControllerDelegate>

@property (strong, nonatomic) CVService *service;

@property (strong, nonatomic) UIDocumentPickerViewController *documentPicker;

@end

@implementation CVMessagesViewController

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.service = [CVService new];
    self.messagesData = [CVMessagesModel new];
    
    self.inputToolbar.contentView.textView.pasteDelegate = self;
    
    [self.navigationItem.backBarButtonItem setTitle:@" "];
   
    [JSQMessagesCollectionViewCell registerMenuAction:@selector(customAction:)];
    [JSQMessagesCollectionViewCell registerMenuAction:@selector(delete:)];
    
    self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeMake(30, 30);
    self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeMake(30, 30);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.delegateModal) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                                                                              target:self
                                                                                              action:@selector(closePressed:)];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.collectionView.collectionViewLayout.springinessEnabled = YES;
}

#pragma mark - Custom menu actions for cells

- (void)didReceiveMenuWillShowNotification:(NSNotification *)notification
{
    UIMenuController *menu = [notification object];
    menu.menuItems = @[ [[UIMenuItem alloc] initWithTitle:@"Custom Action" action:@selector(customAction:)] ];
    
    [super didReceiveMenuWillShowNotification:notification];
}

#pragma mark - Actions

- (void)closePressed:(UIBarButtonItem *)sender
{
    [self.delegateModal didDismissJSQDemoViewController:self];
}

#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
    [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:senderId
                                             senderDisplayName:senderDisplayName
                                                          date:date
                                                          text:text];
    
    [self.messagesData.messages addObject:message];
    
    [self finishSendingMessageAnimated:YES];
    
    [self handleMessage:text];
}

- (void)didPressAccessoryButton:(UIButton *)sender
{
    [self.inputToolbar.contentView.textView resignFirstResponder];
    
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Медиа сообщение"
                                                                        message:nil
                                                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    Welf();
    [controller addAction:[UIAlertAction actionWithTitle:@"Изображение" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        Self();
        
        UIImagePickerController *imagePickerController = [UIImagePickerController new];
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.delegate = self;
        
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }]];
    
    [controller addAction:[UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - JSQMessages CollectionView DataSource

- (NSString *)senderId {
    return @"Клиент";
}

- (NSString *)senderDisplayName {
    return @"Клиент";
}

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.messagesData.messages objectAtIndex:indexPath.item];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didDeleteMessageAtIndexPath:(NSIndexPath *)indexPath
{
    [self.messagesData.messages removeObjectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messagesData.messages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.messagesData.outgoingBubbleImageData;
    }
    
    return self.messagesData.incomingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messagesData.messages objectAtIndex:indexPath.item];
    
    return [self.messagesData.avatars objectForKey:message.senderId];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item % 3 == 0) {
        JSQMessage *message = [self.messagesData.messages objectAtIndex:indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messagesData.messages objectAtIndex:indexPath.item];
    
    // iOS7-style sender name labels
    if ([message.senderId isEqualToString:self.senderId]) {
        return nil;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.messagesData.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
            return nil;
        }
    }
    
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.messagesData.messages count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    JSQMessage *msg = [self.messagesData.messages objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            cell.textView.textColor = [UIColor blackColor];
        }
        else {
            cell.textView.textColor = [UIColor whiteColor];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    return cell;
}

#pragma mark - Custom menu items

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        return YES;
    }
    
    return [super collectionView:collectionView canPerformAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        [self customAction:sender];
        return;
    }
    
    [super collectionView:collectionView performAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)customAction:(id)sender
{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Custom Action", nil)
                                                                        message:nil
                                                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [controller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                   style:UIAlertActionStyleDefault
                                                 handler:nil]];
    
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - ImagePickerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [self handleImageUploading:info];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - JSQMessages collection view flow layout delegate

#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item % 3 == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    // iOS7-style sender name labels
    JSQMessage *currentMessage = [self.messagesData.messages objectAtIndex:indexPath.item];
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.messagesData.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.0f;
}

#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
{
    NSLog(@"Load earlier messages!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped message bubble!");
    
    id<JSQMessageMediaData> mediaItem = ((JSQMessage *)self.messagesData.messages[indexPath.row]).media;
    
    if ([mediaItem isKindOfClass:[CVFileMediaItem class]]) {
        NSURL *url = [NSURL fileURLWithPath:((CVFileMediaItem *)mediaItem).fileURL];
        UIDocumentInteractionController *popup = [UIDocumentInteractionController interactionControllerWithURL:url];
        [popup setDelegate:self];
        [popup presentPreviewAnimated:YES];
    } else if ([mediaItem isKindOfClass:[JSQPhotoMediaItem class]]) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CVImagePresentingViewController *vc = [sb instantiateViewControllerWithIdentifier:@"CVImagePresentingViewController"];
        vc.image = ((JSQPhotoMediaItem *)mediaItem).image;    
        
        vc.modalTransitionStyle = UIModalPresentationPopover;
        [self presentViewController:vc animated:YES completion:NULL];
    }
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation
{
    NSLog(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));
}

#pragma mark - JSQMessagesComposerTextViewPasteDelegate methods

- (BOOL)composerTextView:(JSQMessagesComposerTextView *)textView shouldPasteWithSender:(id)sender
{
    if ([UIPasteboard generalPasteboard].image) {
        // If there's an image in the pasteboard, construct a media item with that image and `send` it.
        JSQPhotoMediaItem *item = [[JSQPhotoMediaItem alloc] initWithImage:[UIPasteboard generalPasteboard].image];
        JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId
                                                 senderDisplayName:self.senderDisplayName
                                                              date:[NSDate date]
                                                             media:item];
        [self.messagesData.messages addObject:message];
        [self finishSendingMessage];
        return NO;
    }
    return YES;
}

#pragma mark - JSQMessagesViewAccessoryDelegate methods

- (void)messageView:(JSQMessagesCollectionView *)view didTapAccessoryButtonAtIndexPath:(NSIndexPath *)path
{
    NSLog(@"Tapped accessory button!");
}

#pragma mark - Message handling

- (void)handleMessage:(NSString *)message
{
    CVCommandRequest *r = [CVCommandRequest new];
    r.message = message;
    
    [self.service executeWithRequest:r
                       responseClass:[CVCommandsResponse class]
                             success:^(CVResponse *response, NSData *data) {
                                 CVCommandsResponse *resp = (CVCommandsResponse *)response;
        
                                 [self handleResponse:resp];
    } failure:^(NSError *error) {
        [self serviceErrorHappened:error];
    }];
}

- (void)handleImageUploading:(NSDictionary<NSString *,id> *)info
{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    PHFetchResult *result = [PHAsset fetchAssetsWithALAssetURLs:@[refURL] options:nil];
    NSString *filename = [[result firstObject] filename];
    
    if (image) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *path = [documentsDirectory stringByAppendingPathComponent:filename];
        
        NSData *data = UIImagePNGRepresentation(image);
        [data writeToFile:path atomically:YES];
        
        [self.messagesData.messages addObject:[[JSQMessage alloc] initWithSenderId:@"Клиент"
                                                                 senderDisplayName:@"Клиент"
                                                                              date:[NSDate date]
                                                                              text:[NSString stringWithFormat:@"File sending: %@", filename]]];
        
        [self finishSendingMessageAnimated:YES];
        
        [self addLoadingMessage];
        
        NSInteger replaceableIndex = self.messagesData.messages.count - 1;
        
        [self.service executeUploadingWithPath:path success:^(CVResponse *response, NSData *data) {
            NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            [self.messagesData.messages replaceObjectAtIndex:replaceableIndex withObject:[[JSQMessage alloc] initWithSenderId:@"Сервер"
                                                                     senderDisplayName:@"Сервер"
                                                                                  date:[NSDate date]
                                                                                  text:result]];
            [self finishSendingMessageAnimated:YES];
        } failure:^(NSError *error) {
            [self.messagesData.messages removeObjectAtIndex:replaceableIndex];
            
            [self serviceErrorHappened:error];
        }];
    } else {
        [self dismissViewControllerAnimated:YES completion:^{
            [self clientErrorHappened:@"Не удалось выбрать изображение"];
        }];
        return;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Response handling

- (void)handleResponse:(CVCommandsResponse *)response
{
    NSString *dataType = response.dataType;
    
    if ([response.code isEqualToString:@"200"]) {
        if ([dataType isEqualToString:@"text"]) {
            [self.messagesData.messages addObject:[[JSQMessage alloc] initWithSenderId:@"Сервер"
                                                                     senderDisplayName:@"Сервер"
                                                                                  date:[NSDate date]
                                                                                  text:response.data]];
            [self finishSendingMessageAnimated:YES];
        } else {
            [self downloadFile:response.data];
        }
    } else {
        [self.messagesData.messages addObject:[[JSQMessage alloc] initWithSenderId:@"Сервер"
                                                                 senderDisplayName:@"Сервер"
                                                                              date:[NSDate date]
                                                                              text:response.responseDescription]];
        [self finishSendingMessageAnimated:YES];
    }
}

- (void)downloadFile:(NSString *)url
{
    [self addLoadingMessage];
    NSInteger replaceableIndex = self.messagesData.messages.count - 1;
    
    [self.service executeDownloadingTaskWithURL:url success:^(NSString *mimeType, NSString *fileURL) {
        
        if ([mimeType containsString:@"audio"]) {
            [self handleFileDownloading:fileURL withItemIndex:replaceableIndex];
        } else if ([mimeType containsString:@"image"]) {
            [self handleImageDownloading:fileURL withItemIndex:replaceableIndex];
        } else {
            [self handleFileDownloading:fileURL withItemIndex:replaceableIndex];
        }
    } failure:^(NSError *error) {
        [self.messagesData.messages removeObjectAtIndex:replaceableIndex];
        
        [self serviceErrorHappened:error];
    }];
}

/*
- (void)handleAudioDownloading:(NSString *)fileURL withItemIndex:(NSInteger)index
{
    [self.messagesData.messages replaceObjectAtIndex:index
                                          withObject:[[JSQMessage alloc] initWithSenderId:@"Сервер"
                                                                        senderDisplayName:@"Сервер"
                                                                                     date:[NSDate date]
                                                                                    media:[[CVFileMediaItem alloc] initWithFileURL:fileURL]]];
    [self finishSendingMessageAnimated:YES];
}
 */

- (void)handleImageDownloading:(NSString *)fileURL withItemIndex:(NSInteger)index
{
    NSURL *url = [NSURL URLWithString:fileURL];
    
    NSData *imageData = [NSData dataWithContentsOfURL:url];
    
    // Save to library
    UIImageWriteToSavedPhotosAlbum([UIImage imageWithData:imageData], nil, nil, nil);
    
    [self.messagesData.messages replaceObjectAtIndex:index
                                          withObject:[[JSQMessage alloc] initWithSenderId:@"Сервер"
                                                                        senderDisplayName:@"Сервер"
                                                                                     date:[NSDate date]
                                                                                    media:[[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageWithData:imageData]]]];
    [self finishSendingMessageAnimated:YES];
}

- (void)handleFileDownloading:(NSString *)fileURL withItemIndex:(NSInteger)index
{
    
    CVFileMediaItem *data = [[CVFileMediaItem alloc] initWithFileURL:fileURL];
    data.appliesMediaViewMaskAsOutgoing = NO;
    [self.messagesData.messages replaceObjectAtIndex:index
                                          withObject:[[JSQMessage alloc] initWithSenderId:@"Сервер"
                                                                        senderDisplayName:@"Сервер"
                                                                                     date:[NSDate date]
                                                                                    media:data]];
    [self finishSendingMessageAnimated:YES];

}

#pragma mark - Other

- (void)clientErrorHappened:(NSString *)errorDescription
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Ошибка"
                                                                             message:errorDescription
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)serviceErrorHappened:(NSError *)error
{
    [self.messagesData.messages addObject:[[JSQMessage alloc] initWithSenderId:@"Сервер"
                                                             senderDisplayName:@"Сервер"
                                                                          date:[NSDate date]
                                                                          text:[error localizedDescription]]];
    [self finishSendingMessageAnimated:YES];
}

- (void)addLoadingMessage
{
    JSQAudioMediaItem *data = [[JSQAudioMediaItem alloc] initWithData:nil];
    data.appliesMediaViewMaskAsOutgoing = NO;
    
    [self.messagesData.messages addObject:[[JSQMessage alloc] initWithSenderId:@"Сервер"
                                                             senderDisplayName:@"Сервер"
                                                                          date:[NSDate date]
                                                                         media:data]];
    
    [self finishSendingMessageAnimated:YES];
}

#pragma mark - UIDocumentInteractionDelegate

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller
{
    return self;
}

@end
