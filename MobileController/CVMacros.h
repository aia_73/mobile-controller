//
//  CVMacros.h
//  MobileController
//
//  Created by Ivan Tsaryov on 15/05/2017.
//  Copyright © 2017 Creactive. All rights reserved.
//

#define Welf() \
typeof(self) __weak welf = self
#define Self() \
typeof(self) __strong self = welf
#define Uelf() \
typeof(self) __unsafe_unretained uelf = self
