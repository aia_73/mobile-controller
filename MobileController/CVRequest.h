//
//  CVRequest.h
//  MobileController
//
//  Created by Ivan Tsaryov on 30/04/17.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface CVRequest : JSONModel

@property (strong, nonatomic) NSString *url;

@end
