//
//  CVSettingsViewController.m
//  MobileController
//
//  Created by Ivan Tsaryov on 12/05/2017.
//  Copyright © 2017 Creactive. All rights reserved.
//

#import "CVSettingsViewController.h"

@interface CVSettingsViewController ()

@property (weak, nonatomic) IBOutlet UITextField *endpointTextField;

@end

@implementation CVSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.endpointTextField.text = self.serviceEndpoint;
}

- (NSString *)serviceEndpoint
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    return [defaults stringForKey:@"kCVServerEndpoint"];
}

#pragma mark - IBAction

- (IBAction)onSaveButtonTap:(UIButton *)sender {
    
    if (self.endpointTextField.text.length > 0) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        [defaults setObject:self.endpointTextField.text forKey:@"kCVServerEndpoint"];
        [defaults synchronize];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
